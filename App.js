import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Home from './screens/home';
import About from './screens/about';
import Settings from './screens/settings';
import Details from './screens/details';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

function HomeStack() {
	return (
		<Stack.Navigator>
			<Stack.Screen name="Home" component={Home} />
			<Stack.Screen name="Details" component={Details} />
		</Stack.Navigator>
	);
}

function HomeTab() {
	return (
		<Tab.Navigator>
			<Tab.Screen name="Home" component={HomeStack} />
			<Tab.Screen name="About" component={About} />
		</Tab.Navigator>
	);
}

function App() {
	return (
		<NavigationContainer>
			<Drawer.Navigator initialRouteName="Home">
				<Drawer.Screen name="Home" component={HomeTab} />
				<Drawer.Screen name="Settings" component={Settings} />
			</Drawer.Navigator>
		</NavigationContainer>
	);
}

export default App;
