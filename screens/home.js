import React from 'react';

import { globalStyles } from '../styles/global.js';
import { View, Text, Button } from 'react-native';

export default function Home({ navigation }) {
	return (
		<View style={globalStyles.main}>
			<Text style={globalStyles.text}>this is the home screen</Text>
			<Button title="Go to the details!" onPress={() => navigation.navigate('Details')} />
		</View>
	);
}
