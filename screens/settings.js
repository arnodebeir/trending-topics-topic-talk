import React from 'react';

import { globalStyles } from '../styles/global.js';
import { View, Text } from 'react-native';

export default function Settings() {
	return (
		<View style={globalStyles.main}>
			<Text style={globalStyles.text}>this is the settings screen</Text>
		</View>
	);
}
