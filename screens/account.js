import React from 'react';

import { globalStyles } from '../styles/global.js';
import { View, Text, Button } from 'react-native';

export default function Account({ navigation }) {
	return (
		<View style={globalStyles.main}>
			<Text style={globalStyles.text}>this is the account screen</Text>
			<Button title="about" onPress={() => navigation.navigate('About')} />
		</View>
	);
}
