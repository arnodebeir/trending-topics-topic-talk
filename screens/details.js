import React from 'react';

import { globalStyles } from '../styles/global.js';
import { View, Text } from 'react-native';

export default function Details() {
	return (
		<View style={globalStyles.main}>
			<Text style={globalStyles.text}>this is the details screen</Text>
		</View>
	);
}
