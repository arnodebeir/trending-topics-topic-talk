import { StyleSheet, Dimensions, StatusBar, Platform } from 'react-native';
import { colors } from '../helpers/colors';

export const globalStyles = StyleSheet.create({
	main: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	}
});
