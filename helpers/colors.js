export const colors = {
	purple: '#D982AB',
	orange: '#F26430',
	brown: '#BF472C',
	pink: '#F28585',
	black: '#0D0D0D'
};
